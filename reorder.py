# "Duplicate and Reorder" Anki add-on
# Copyright (C) 2020 Régis Martinez (regis.martinez3@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from aqt import browser
from aqt import mw
from aqt.qt import *
from aqt.utils import showWarning, tooltip
from anki.browser import BrowserConfig

from . import helpers
from .helpers import Direction, Sort



def moveNote(browser, direction):

   # Make sure to update the item displayed before starting : when cards or notes are deleted,
   # Anki leaves a "(deleted)" item in the listview, which then make the algorithm raise a 
   # "Note not found" exception (in swap_note_ids), of course. Simplest way for me here is to
   # just refresh the view to avoid that. Another solution would be to somehow check if the 
   # notes we are swaping are not deleted, otherwise ... otherwise what ? refresh the view ? 
   # fine the closest valid item ? Not trivial. I'll go with that solution for now.
   browser.search()

   db_helper = helpers.DbHelper(browser)
   listView = helpers.CurrentViewList(browser)
   if listView.get_sorting() == Sort.DESCENDING:
      direction = Direction.UP if direction == Direction.DOWN else Direction.DOWN

   selected_note_ids = listView.get_selected_note_ids()
   all_note_ids = listView.get_note_ids()

   # Set checkpoint
   mw.checkpoint("Shift Card(s)")
   mw.progress.start()

   notes_to_flush = []
   new_selected_ids = []

   # keeps track of closest unselected note (with which we'll switch the next selected note with meet)
   previous_switchable_note = None

   # Traverse the note list, switching each selected note with previous available (i.e. unselected)
   # note (if any)
   for note_id in all_note_ids if direction == Direction.UP else reversed(all_note_ids):
      if note_id in selected_note_ids:
         if previous_switchable_note != None:
            db_helper.swap_note_ids(note_id, previous_switchable_note)

            # put new note id in the list of notes to be newly selected
            new_selected_ids.append(previous_switchable_note)

            # previous_switchable_note is moved note
            listView.update_due_if_new(previous_switchable_note)

            # put modified notes in list of cards to mark as sync
            notes_to_flush.append(note_id)
            notes_to_flush.append(previous_switchable_note)

            previous_switchable_note = note_id
         else:
            new_selected_ids.append(note_id)
      else:
         previous_switchable_note = note_id

   # Mark notes and cards as modify and set for sync
   for note_id in notes_to_flush:
      note = browser.col.get_note(note_id)
      note.usn = browser.col.usn()
      for card in note.cards():
         card.usn = browser.col.usn()
         card.flush()
      note.flush()

   browser.col.save()

   # Reset collection and main window
   mw.progress.finish()
   mw.col.reset()
   mw.reset()
   browser.onSearchActivated()

   tooltip("Card(s) shifted")

   # Only in the case we are in Note mode, reselect the shifted notes
   # Selection is handled automatically in Card mode : I suspect it keeps track of selection 
   # using card ids, whereas it uses note ids in Note mode (thus messing not updating selection 
   # because we swap note ids, without changing them)
   if (browser.table.is_notes_mode()):
      rows_to_reselect = browser.table._model.get_item_rows(new_selected_ids)
      browser.table.clear_selection()
      browser.table._select_rows(rows_to_reselect)


def moveNoteUp(browser):
   moveNote(browser, Direction.UP)

def moveNoteDown(browser):
   moveNote(browser, Direction.DOWN)

# Deactivated cause feature actually already in Anki
# 
# 1. **Reset new cards Due date** according to current *Created* date ordering
#    - select note(s), then `Alt+Shift+R`
# def resetNewDueOrder(browser):
#    mw.checkpoint("Reset new cards Due order")
#    mw.progress.start()
#
#    sortedInReverse = browser.col.conf['sortBackwards']
#
#    for offset, card_id in enumerate(browser.model.cards):
#       card = browser.col.getCard(card_id)
#       if card.type == 0: # only new cards
#          card.due = (1000000 + offset) if not sortedInReverse else 1000000 + len(browser.model.cards) - 1 - offset
#          card.flush()
#
#    # Reset collection and main window
#    browser.model.endReset()
#    mw.col.reset()
#    mw.reset()
#    mw.progress.finish()
