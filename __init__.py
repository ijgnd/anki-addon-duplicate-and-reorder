# "Duplicate and Reorder" Anki add-on
# Copyright (C) 2020 Régis Martinez (regis.martinez3@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>.


#  Duplicate And Reorder(DNR) addon



from aqt import browser
from aqt import mw
from aqt.qt import *
from anki import hooks
from anki.browser import BrowserConfig
from aqt.utils import shortcut, tooltip

from .config import getUserOption
from . import duplicate
from . import reorder

browser.Browser.dnrDuplicateCards = duplicate.duplicateCards
browser.Browser.dnrMoveNoteUp = reorder.moveNoteUp
browser.Browser.dnrMoveNoteDown = reorder.moveNoteDown
# browser.Browser.dnrResetNewDueOrder = reorder.resetNewDueOrder

# Set menus and corresponding actions
def setupDNRActions(browser):

   # Duplicate current selected card(s)
   dnrDuplicateCardsAction = QAction("Duplicate", browser)
   scut = getUserOption("Shortcut: Duplicate", "Alt+D")
   dnrDuplicateCardsAction.setShortcut(shortcut(scut))
   dnrDuplicateCardsAction.triggered.connect(browser.dnrDuplicateCards)
   browser.form.dnrDuplicateCardsAction = dnrDuplicateCardsAction

   # Move Up current selected card(s)
   dnrMoveUpAction = QAction("Move up", browser)
   scut = getUserOption("Shortcut: Move up", "Alt+Up")
   dnrMoveUpAction.setShortcut(shortcut(scut))
   dnrMoveUpAction.triggered.connect(browser.dnrMoveNoteUp)
   browser.form.dnrMoveUpAction = dnrMoveUpAction
   
   # Move Down current selected card(s)
   dnrMoveDownAction = QAction("Move down", browser)
   scut = getUserOption("Shortcut: Move down", "Alt+Down")
   dnrMoveDownAction.setShortcut(shortcut(scut))
   dnrMoveDownAction.triggered.connect(browser.dnrMoveNoteDown)
   browser.form.dnrMoveDownAction = dnrMoveDownAction
   
   # # Reset 'Due' order or new cards, to follow current "created" order
   # dnrResetNewDueOrder = QAction("Reset new cards Due", browser)
   # dnrResetNewDueOrder.setShortcut(shortcut("Alt+Shift+R"))
   # dnrResetNewDueOrder.triggered.connect(browser.dnrResetNewDueOrder)
   # browser.form.dnrResetNewDueOrder = dnrResetNewDueOrder
   
   browser.form.menu_Notes.addSeparator()
   browser.form.menu_Notes.addAction(dnrDuplicateCardsAction)
   browser.form.menu_Notes.addAction(dnrMoveUpAction)
   browser.form.menu_Notes.addAction(dnrMoveDownAction)
   # browser.form.menu_Notes.addAction(dnrResetNewDueOrder)
   
   # Set the actions active only if the cards are sorted by creations date. 
   # This is necessary because the reposition is done considering the current
   # ordering in the browser
   dnrUpdateActions(browser, browser.table.is_notes_mode())


def dnrOnSortChanged(self, idx, ord):
   dnrUpdateActions(self.browser, self.browser.table.is_notes_mode())


def dnrOnBrowserModeChanged(self, checked):
   dnrUpdateActions(self, checked)


# Set the actions active only if the notes are sorted by creations date. 
# This is necessary because the reposition is done considering the current
# ordering in the browser
def dnrUpdateActions(browser, is_note_mode):
   model = browser.table._model
   sort_column = model._state.sort_column
   isCreatedSort = sort_column in ['noteCrt', 'ctimecrtn', 'nid']

   browser.form.dnrDuplicateCardsAction.setEnabled(isCreatedSort)
   browser.form.dnrMoveUpAction.setEnabled(isCreatedSort)
   browser.form.dnrMoveDownAction.setEnabled(isCreatedSort)
   # self.browser.form.dnrResetNewDueOrder.setEnabled(isCreatedSort)

hooks.addHook("browser.setupMenus", setupDNRActions)

# Ask notifications for sorting change
browser.table.Table._on_sort_column_changed = hooks.wrap(
    browser.table.Table._on_sort_column_changed, dnrOnSortChanged
)

browser.Browser.on_table_state_changed = hooks.wrap(
    browser.Browser.on_table_state_changed, dnrOnBrowserModeChanged
)

# Notes to myself :
# 
# Anki Debug Console : switch to EN keyboard : ctrl+shift+M :
# import sys
# from aqt import mw
# sys.path.insert(0,mw.pm.addonFolder())
# import duplicate_and_reorder
# reload(duplicate_and_reorder)
# 
# - browser.col.get_card(id) : get the 'Card' object for given card id
# - browser.mw : the fucking main window, of type "AnkiQT"
# - browser.table._model.get_card_row(card_id) : row number of the card in current displayed deck
# - browser.table._model.items : Currently displayed items row indexes seem to be in 
# - browser.table._model._items[row] : itemId at given row
# - browser.table._model._state.get_note(itemId) : get note object from item object

# Get order of sort
# - reverse_key = BrowserConfig.sort_backwards_key(browser.table.is_notes_mode())
# - sortedInReverse = browser.col.get_config(reverse_key)


# in previous versions :
# - browser.model.cards : sequence of card ids currently displayed in browser, in display order
# - browser.model.index(row, col) : QModelIndex object at given position (we don't really care about the col in our case)
#     e.g. :   previousRowQIndex = browser.model.index(row - 1, 0)
# - browser.form.tableView.selectionModel() : QT selection model, contains the QModelItem objects, indexed by QModelIndex ...
# - QModelIndex::row() : row number of the element with given QModelIndex 
#     e.g. :   previousRowQIndex = browser.model.index(row - 1, 0)
#              if browser.form.tableView.selectionModel().isSelected(previousRowQIndex): ...
#
# Useful resources
# 
# https://chrisk91.me/2018/02/13/Setting-up-VSCode-for-Anki-addon-development.html
# https://www.juliensobczak.com/write/2016/12/26/anki-scripting.html
# https://addon-docs.ankiweb.net/#/
# https://github.com/ankitects/anki-addons
# https://github.com/ankitects/anki
#
# https://www.reddit.com/r/Anki/comments/9n3l68/is_there_any_sort_of_api_documentation_for_addon/
# https://www.reddit.com/r/Anki/comments/961cj8/documentation_for_creating_addons/
# 


